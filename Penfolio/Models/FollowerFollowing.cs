﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("FollowerFollowing")]
    public partial class FollowerFollowing
    {
        [Key]
        [Required]
        public int FollowerFollowingID { get; set; }

        [Required]
        public int FollowerID { get; set; }

        public int? ProfileID { get; set; }

        public int? FolderID { get; set; }

        public int? SeriesID { get; set; }

        public int? FormatID { get; set; }

        public int? GenreID { get; set; }

        [ForeignKey("FollowerID")]
        public virtual PenProfile Follower { get; set; }

        [ForeignKey("ProfileID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PenProfile> ProfilesFollowing { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Folder> FoldersFollowing { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Series> SeriesFollowing { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormatTag> FormatTagsFollowing { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GenreTag> GenreTagsFollowing { get; set; }
    }
}