﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("WritingSeries")]
    public partial class WritingSeries
    {
        [Key]
        [Required]
        public int WritingSeriesID { get; set; }

        [Required]
        public int SeriesID { get; set; }

        [Required]
        public int WritingID { get; set; }

        public int? PreviousWritingID { get; set; }

        public int? NextWritingID { get; set; }

        [Required]
        public bool IsStandAlone { get; set; }

        [ForeignKey("SeriesID")]
        public virtual Series Series { get; set; }

        public virtual Writing Writing { get; set; }
    }
}