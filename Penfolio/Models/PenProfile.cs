﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("PenProfile")]
    public partial class PenProfile
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PenProfile()
        {
            ProfileWritings = new HashSet<WritingProfile>();
            Folders = new HashSet<Folder>();
            CritiqueGiven = new HashSet<Critique>();
            CritiqueNotificationSettings = new HashSet<CritiqueGiver>();
            Friends = new HashSet<Friendship>();
            FriendRequestsSent = new HashSet<FriendRequest>();
            FriendRequestsReceived = new HashSet<FriendRequest>();
            PendingAccessRequests = new HashSet<AccessRequest>();
            CommentsReceived = new HashSet<Comment>();
            CommentsMade = new HashSet<Comment>();
            FlaggedComments = new HashSet<CommentFlag>();
            LikesMade = new HashSet<Likes>();
            LikesReceived = new HashSet<Likes>();
            Following = new HashSet<FollowerFollowing>();
            Followers = new HashSet<FollowerFollowing>();
        }

        [Key]
        [Required]
        public int ProfileID { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string ProfileName { get; set; }

        [Required]
        public int RoleID { get; set; }

        [Required]
        public int AccessPermissionID { get; set; }

        [Required]
        public bool UseSecondaryRoleName { get; set; }

        [Required]
        public string URLString { get; set; }

        [Required]
        public bool IsMainProfile { get; set; }

        public string ProfileDescription { get; set; }

        public byte[] ProfilePhoto { get; set; }

        [Required]
        public bool Verified { get; set; }

        public virtual PenUser PenUser { get; set; }

        public virtual PenRole PenRole { get; set; }

        public virtual AccessPermission AccessPermission { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<WritingProfile> ProfileWritings { get; set; }

        [ForeignKey("OwnerID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Folder> Folders { get; set; }

        [ForeignKey("CriticID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Critique> CritiqueGiven { get; set; }

        [ForeignKey("CriticID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CritiqueGiver> CritiqueNotificationSettings { get; set; }

        [ForeignKey("FirstFriendID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Friendship> Friends { get; set; }

        [ForeignKey("RequesterProfileID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FriendRequest> FriendRequestsSent { get; set; }

        [ForeignKey("RequesteeProfileID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FriendRequest> FriendRequestsReceived { get; set; }

        [ForeignKey("RequesterID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccessRequest> PendingAccessRequests { get; set; }

        [ForeignKey("ProfileID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> CommentsReceived { get; set; }

        [ForeignKey("CommenterID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> CommentsMade { get; set; }

        [ForeignKey("FlaggerID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CommentFlag> FlaggedComments { get; set; }

        [ForeignKey("LikerID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Likes> LikesMade { get; set; }

        [ForeignKey("ProfileID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Likes> LikesReceived { get; set; }

        [ForeignKey("FollowerID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FollowerFollowing> Following { get; set; }

        [ForeignKey("ProfileID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FollowerFollowing> Followers { get; set; }
    }
}