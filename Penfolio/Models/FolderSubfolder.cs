﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("FolderSubfolder")]
    public partial class FolderSubfolder
    {
        [Key]
        [Required]
        public int FolderSubfolderID { get; set; }

        [Required]
        public int FolderID { get; set; }

        [Required]
        public int SubfolderID { get; set; }

        public virtual Folder Folder { get; set; }

        [ForeignKey("SubfolderID")]
        public virtual Folder Subfolder { get; set; }
    }
}