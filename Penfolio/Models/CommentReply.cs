﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("CommentReply")]
    public partial class CommentReply
    {
        [Key]
        [Required]
        public int CommentReplyID { get; set; }

        [Required]
        public int CommentID { get; set; }

        [Required]
        public int ReplyID { get; set; }

        [ForeignKey("CommentID")]
        public virtual Comment Comment { get; set; }

        [ForeignKey("ReplyID")]
        public virtual Comment Reply { get; set; }
    }
}