﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("Likes")]
    public partial class Likes
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Likes()
        {
            LikedProfile = new HashSet<PenProfile>();
            LikedWriting = new HashSet<Writing>();
            LikedFolder = new HashSet<Folder>();
            LikedSeries = new HashSet<Series>();
            LikedComment = new HashSet<Comment>();
        }

        [Key]
        [Required]
        public int LikeID { get; set; }

        [Required]
        public int LikerID { get; set; }

        [Required]
        public bool IsAnonymous { get; set; }

        [Required]
        public DateTime LikeDate { get; set; }

        public int? ProfileID { get; set; }

        public int? WritingID { get; set; }

        public int? FolderID { get; set; }

        public int? SeriesID { get; set; }

        public int? CommentID { get; set; }

        [ForeignKey("LikerID")]
        public virtual PenProfile Liker { get; set; }

        [ForeignKey("ProfileID")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PenProfile> LikedProfile { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Writing> LikedWriting { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Folder> LikedFolder { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Series> LikedSeries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> LikedComment { get; set; }
    }
}