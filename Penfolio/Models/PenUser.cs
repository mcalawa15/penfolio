﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Penfolio.Models
{
    [Table("AspNetUsers")]
    public partial class PenUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PenUser()
        {
            PenProfiles = new HashSet<PenProfile>();
            UsersBlockedBy = new HashSet<PenUser>();
            BlockedUsers = new HashSet<PenUser>();
        }

        [Key]
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Email { get; set; }

        public DateTime? Birthdate { get; set; }

        [Required]
        public bool UseLowDataMode { get; set; }

        [Required]
        public int Strikes { get; set; }

        [Required]
        public bool HasAdminPrivileges { get; set; }

        [ForeignKey("Username")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PenProfile> PenProfiles { get; set; }

        [ForeignKey("BlockedUsername")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<PenUser> UsersBlockedBy { get; set; }

        [ForeignKey("BlockingUser")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<PenUser> BlockedUsers { get; set; }
    }
}